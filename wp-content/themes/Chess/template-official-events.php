<?php
/*
  Template Name: Official Events
*/
get_header();
global $imic_options;
$custom_home = get_post_custom(get_the_ID());
$home_id = get_the_ID();
$pageOptions = imic_page_design('', 8); //page design options
imic_sidebar_position_module();
get_template_part('flex-slider');

?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_left_none announce">
                                Official Events
                            </div>
                            <hr>
                        </div>

                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover">
                            <tbody>
                            <tr>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Location</th>
                                <th></th>
                            </tr>
                            <tr>
                                <td><a class="player-table-values" <a href="">National Cities - 2018 - 189826 / WB / 2018</a></td>
                                <td class="date">01 Aug - 05 Aug</td>
                                <td class="location">Siliguri Indoor stadium, Desh Bandhu Para, Siliguri, Dist. Darjeeling ,Pin 734004
                                </td>
                                <td class="url"><a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a> <a class="down" href=""><i class="fa fa-download" aria-hidden="true"></i></a></td>
                            </tr>

                            <tr>
                                <td><a class="player-table-values" href="" title="Delhi International Open 2018 – 154104 / 154105 / 154106 / DEL / 2018">National Under - 25 ( Open & Girls ) - 2018 - 192951 / MP / 2018</a></td>
                                <td class="date">21 Aug - 27 Aug</td>
                                <td class="location">Hotel Kanta Shravan Palace, Hotel Kanta Shravan Palace, BHEL, Bhopal, MP</td>
                                <td class="url"><a href=""><i class="fa fa-envelope" aria-hidden="true"></i></a> <a class="down" href=""><i class="fa fa-download" aria-hidden="true"></i></a></td>
                            </tr>


                            </tbody>
                        </table>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href=""> ALL EVENTS </a>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">OTHER EVENTS</a>
                        </div>
                    </div>
                    <form>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group padd_left_right_none padd_top_20">
                            <select class="form-control" id="sel1">
                                <option value="0">Select a Reg Status</option>
                                <option value="y">Active</option>
                                <option value="p">Processing</option>
                                <option value="n">Inactive</option>
                            </select>
                            <button type="button" class="btn btn-warning">Filter</button>
                            <input class="btn btn-info" onclick="loadpage();" value="Clear Filter" name="clear" class="event_filter_button"
                                   type="reset">
                        </div>
                    </form>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://dev-cbe-chess.pantheonsite.io/twin-international/"> <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
?>