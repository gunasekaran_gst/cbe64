<?php
/*
  Template Name: FAQ
*/
get_header();
global $imic_options;
$custom_home = get_post_custom(get_the_ID());
$home_id = get_the_ID();
$pageOptions = imic_page_design('', 8); //page design options
imic_sidebar_position_module();
get_template_part('flex-slider');

?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_left_none announce padd_botton_20 padd_top_10">
                                Table Contents
                            </div>
                        </div>

                        <main id="main" class="site-main" role="main">

                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_botton_10">
                                <li><a class="player-table-values" href="#faq-registration"> 1. AICF / FIDE Registration</a></li>
                            </ul>

                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_botton_10">
                                <li><a class="player-table-values" href="#faq-rating"> 2. Rating &amp; Titles</a></li>
                            </ul>

                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_botton_10">
                                <li><a class="player-table-values" href="#faq-tournament"> 3. Tournament Related</a></li>
                            </ul>

                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_botton_10">
                                <li><a class="player-table-values" href="#faq-foreign"> 4. Foreign Travel</a></li>
                            </ul>

                            <ul class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10">
                                <li><a class="player-table-values" href="#faq-other"> 5. Other Questions</a></li>
                            </ul>

                            <div id="faq-registration" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                                1. AICF / FIDE Registration
                            </div>

                            <div id="faq-17744" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-BymistakeIwronglyrenewedanotherplayersidwhattodo"
                                     class="arconix-faq-title faq-closed col-lg-12 col-md-12 col-sm-12 col-xs-12">By mistake I wrongly renewed another player’s
                                    id, what to do?
                                </div>
                                <div class="arconix-faq-content faq-closed col-lg-12 col-md-12 col-sm-12 col-xs-12"><p>Please send the
                                        payment proof, we will correct the mistake.</p></div>
                            </div>
                            <div id="faq-17712" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-HowcanIchangemyFIDEflagtoIndianandexplaintheprocedure"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">How can I change my FIDE flag to Indian and
                                    explain the procedure?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Send a request to
                                        AICF to change the flag. Necessary FIDE fee is applicable (please refer FIDE
                                        website).</p></div>
                            </div>
                            <div id="faq-17704" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-HowtoregisterwithAICFwhetheritiscompulsorytoregisterwithstatedistrict"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">How to register with AICF, whether it is
                                    compulsory to register with state/district?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>By online. Many
                                        state associations expect the players to be registered with them also, while
                                        registering with AICF, kindly check with your State Associations.</p>
                                    <p></p></div>
                            </div>
                            <div id="faq-17736" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IamaratedplayeranddidnotplayinanyratedtournamentforafewyearsIdonotremembermyfideidandIdonotfindmynameinthefideratinglist"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I am a rated player and did not play in any
                                    rated tournament for a few years. I do not remember my fide id and I do not find my
                                    name in the fide rating list.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Please let us know
                                        you’re the name of the tournaments you have participated recently(if the rating
                                        has gone below 1000, the player will become unrated).</p></div>
                            </div>
                            <div id="faq-17716" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavealreadydonemyAICFregistrationPleaseletmeknowthestatusofmyFIDEidIhavetoplayinaratedtournamentsoon"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have already done my AICF registration.
                                    Please let me know the status of my FIDE id. I have to play in a rated tournament
                                    soon.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>AICF will take 3
                                        working days to create FIDE id, please send a mail to AICF with Subject: FIDE ID
                                        CREATION and we will do it asap.</p></div>
                            </div>
                            <div id="faq-17714" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavedonemyAICFregistrationonlineMyregistrationisstillpendingPleaselookintoitIhavepaidmoneyonlineTheamounthasbeendebitedfrommyaccountbutnotshowingnecessarycreditinthesystem"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have done my AICF registration online.My
                                    registration is still pending. Please look into it. (I have paid money online. The
                                    amount has been debited from my account but not showing necessary credit in the
                                    system?)
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Please send your
                                        transaction id and date to do the needful.</p></div>
                            </div>
                            <div id="faq-17718" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavepaidmyplayerregistrationArbiterregistrationtwiceIsitpossibletoadjusttheexcessamountpaidfornextyear"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have paid my player registration/Arbiter
                                    registration twice, Is it possible to adjust the excess amount paid for next year ?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>We don’t have any
                                        option to adjust the money for the next year. However, we can return the extra
                                        money paid by you. Please share your bank details to process necessary
                                        refund.</p></div>
                            </div>
                            <div id="faq-17734" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavetwoFIDEidswhattodo" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have two
                                    FIDE ids what to do?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Send the details
                                        of both the FIDE Ids to <a href="">
                                           </a> with a copy to <a
                                                href=""> </a>.
                                        After the payment of necessary fees the performance of both the Ids will be
                                        merged and one FIDE Id will be deleted. FIDE will charge Euros 50 for the same.
                                    </p></div>
                            </div>
                            <div id="faq-17710" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IregisterthroughFIDEonlinearenacanIparticipateinratingtournamentifyeswhatistheprocedureIfnoWhy"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I register through FIDE online arena, can I
                                    participate in rating tournament, if yes, what is the procedure. If no, Why?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>You may directly
                                        participate in the rating tournament with your online arena FIDE Id. But you
                                        should be a registered player of AICF</p></div>
                            </div>
                            <div id="faq-17740" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-MyphotoiswronginAICFFIDE" class="arconix-faq-title faq-closed">My photo is
                                    wrong in AICF / FIDE.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Send your correct
                                        photo to <a href="">
                                           </a> mentioning your AICF &amp; FIDE Id for
                                        correcting the same.</p></div>
                            </div>

                            <div id="faq-rating" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                                2. Rating &amp; Titles
                            </div>

                            <div id="faq-17698" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-HowdoIgetrating" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">How do I get
                                    rating?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>For unrated
                                        players’ the International Rating can be obtained by playing in FIDE rated
                                        tournaments .For criteria and procedure to get a rating kindly see the link:</p>
                                    <p><a href=""
                                          target="_blank" rel="noopener"></a>
                                    </p></div>
                            </div>
                            <div id="faq-17708" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavecompletedmyIMGMWGMWIMFAIAtitlerequirementsWhatisthefeeandwhatarethepaperstobesubmitted"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have completed my IM/GM/WGM/WIM/FA/IA title
                                    requirements. What is the fee and what are the papers to be submitted?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>The fee for the
                                        Title application is subject to change based on FX rates. Please write a mail to
                                        AICF and confirm the amount to be paid. Regarding submission of norms, all the
                                        norm certificates, cross table (for GM/IM/WGM/WIM) or IT3 (for IA/FA) with duly
                                        filled title page (application) should be forwarded through your state chess
                                        association for consideration.</p></div>
                            </div>
                            <div id="faq-17738" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-MyK-factoriswrong" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">My K-factor is
                                    wrong.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Till your age is
                                        below 18 and the rating is below 2300 your K factor will be For the players
                                        above 18 years of age, once the player played 30 rated games the K-factor will
                                        be automatically changed. If there is any other problem, Please let us know your
                                        FIDE id and Date of birth</p></div>
                            </div>
                            <div id="faq-17706" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-MyratingiswronginthefidewebsiteWhatIshoulddo"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">My rating is wrong in the fide website. What I
                                    should do?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Send your details
                                        with FIDE Id, the event participated and a brief report on what basis you think
                                        that the rating was wrong.</p></div>
                            </div>

                            <div id="faq-tournament" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                                3.  Tournament Related
                            </div>
                            <div id="faq-17722" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-ForWorldCadetRapidblitzandsomeothereventstherearenocorrespondingNationalsPleaseenrolmyentryIwillpaythenecessaryfees"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">For World Cadet Rapid/blitz and some other
                                    events, there are no corresponding Nationals. Please enrol my entry. I will pay the
                                    necessary fees.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Players who have
                                        participated in the corresponding age group National Chess Championships will be
                                        allowed to take part in the FIDE events.</p></div>
                            </div>
                            <div id="faq-17720" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavenotparticipatedinthelastyearNationalchampionshipduetoschoolexamillnesscalamityetcPleaseallowmetotakepartintheAsianWorldCommonwealthetc"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have not participated in the last year
                                    National championship due to school exam/illness/calamity etc. Please allow me to
                                    take part in the Asian/World/Commonwealth etc.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>It is subject to
                                        approval of the Hon. Secretary, please send a mail to AICF.</p></div>
                            </div>
                            <div id="faq-17732" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IsitcompulsorytotakepartinstatechampionshipsinordertoparticipateinNationals"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">Is it compulsory to take part in state
                                    championships in order to participate in Nationals?
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Many state
                                        associations have such a rule. To participate in Nationals, recommendation from
                                        the State is compulsory.</p></div>
                            </div>
                            <h3 id="faq-travel" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-term-title arconix-faq-term-travel">
                              </h3>

                            <div id="faq-foreign" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                                4. Foreign Travel
                            </div>

                            <div id="faq-17728" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavenotreceivedmyTADAVisafeeformytravelintheyear8230Ihavesubmittedallaccountsforreimbursementlongback"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have not received my T.A./D.A./Visa fee for
                                    my travel in the year….. I have submitted all accounts for reimbursement long back.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Please let us know
                                        the name of event and amount, we shall check with our records and accordingly
                                        process the necessary refund.</p></div>
                            </div>

                            <div id="faq-other" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none announcess padd_botton_20 padd_top_10">
                                 5. Other Questions
                            </div>

                            <div id="faq-17730" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavecalledAICFofficeseveraltimesoverphonebutthereisnoproperresponse"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have called AICF office several times over
                                    phone but there is no proper response.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Please send your
                                        query by email under subject IMPORTANT QUERY. It will be answered within 2
                                        working days.</p></div>
                            </div>
                            <div id="faq-17725" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-IhavegivenmyapologyletterbutmynameisnotclearedfromtheCAIlistandstillreflectinginthesystem"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">I have given my apology letter but my name is
                                    not cleared from the CAI list and still reflecting in the system.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>Please forward the
                                        mail/copy of the letter by mail to do the needful on priority.</p></div>
                            </div>
                            <div id="faq-17742" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-wrap">
                                <div id="faq-PleasesendAICFchronicletomyaddressIwillthepaythefee"
                                     class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-title faq-closed">Please send AICF chronicle to my address, I
                                    will the pay the fee.
                                </div>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 arconix-faq-content faq-closed"><p>You can download
                                        AICF chronicle from our website aicf.in. No Printed chronicles are
                                        available.</p></div>
                            </div>

                        </main>

                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none padd_top_20">

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://dev-cbe-chess.pantheonsite.io/twin-international/"> <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
?>