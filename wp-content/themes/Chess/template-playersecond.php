<?php
/*
  Template Name: Playersecond
*/
get_header();
global $imic_options;
$custom_home = get_post_custom(get_the_ID());
$home_id = get_the_ID();
$pageOptions = imic_page_design('', 8); //page design options
imic_sidebar_position_module();
get_template_part('flex-slider');

?>
<?php
while (have_posts()) : the_post(); ?> <!--Because the_content() works only inside a WP Loop -->
    <?php the_content(); ?> <!-- Page Content -->
<?php
endwhile;
wp_reset_query(); //resetting the page query
?>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none ">
                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 padd_left_none announce">
                                101-200 Players
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12  padd_left_none announce">
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12  padd_left_none announce">
                                <a class="next" href="http://dev-cbe-chess.pantheonsite.io/player-search/"><< Prev</a>
                            </div>
                            <hr>
                        </div>

                        <table class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_none table table-hover">
                            <tbody>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Titles</th>
                                <th>State</th>
                                <th>Ratings</th>
                                <th>Reg Status</th>
                                <th>Gender</th>
                            </tr>
                            <tr>
                                <td class="player-table-value">101</td>
                                <td><a class="player-table-values" href="">Ravichandran,
                                        Siddharth</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2380</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">102</td>
                                <td><a class="player-table-values" href="">Ravisekhar, Raja</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2380</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">103</td>
                                <td><a class="player-table-values" href="">Muthaiah AL</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2378</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">104</td>
                                <td><a class="player-table-values" href="">Anwesh, Upadhyaya</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2377</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">105</td>
                                <td><a class="player-table-values" href="">Rathnakaran, K.</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Kerala</td>
                                <td class="player-table-value">2376</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">106</td>
                                <td><a class="player-table-values" href="">Praneeth Surya K</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2375</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">107</td>
                                <td><a class="player-table-values" href="">Shyaamnikhil P</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2375</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">108</td>
                                <td><a class="player-table-values" href="">Prathamesh, Sunil
                                        Mokal</a></td>
                                <td class="player-table-value">IM,&nbsp;FT</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2371</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">109</td>
                                <td><a class="player-table-values" href="">Arjun Kalyan</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2370</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">110</td>
                                <td><a class="player-table-values" href="">Kulkarni, Rakesh</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2363</td>
                                <td class="player-table-value">Processing</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">111</td>
                                <td><a class="player-table-values" href="">Soumya,
                                        Swaminathan</a></td>
                                <td class="player-table-value">WGM,&nbsp;WGM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2361</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">112</td>
                                <td><a class="player-table-values" href="">Palit, Somak</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2360</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">113</td>
                                <td><a class="player-table-values" href="">Ravi, Teja S.</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2360</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">114</td>
                                <td><a class="player-table-values" href="">Vijayalakshmi,
                                        Subbaraman</a></td>
                                <td class="player-table-value">IM,&nbsp;WGM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2359</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">115</td>
                                <td><a class="player-table-values" href="">Soham Das</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2359</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">116</td>
                                <td><a class="player-table-values" href="">Abhishek, Kelkar</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2359</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">117</td>
                                <td><a class="player-table-values" href="">Sai Agni Jeevitesh,
                                        J</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2358</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">118</td>
                                <td><a class="player-table-values" href="">Rajdeep Sarkar</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2356</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">119</td>
                                <td><a class="player-table-values" href="">Khosla, Shiven</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2356</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">120</td>
                                <td><a class="player-table-values" href="">Srinath, Rao S.V.</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Chhattisgarh</td>
                                <td class="player-table-value">2355</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">121</td>
                                <td><a class="player-table-values" href="">Murugan,
                                        Krishnamoorthy</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2355</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">122</td>
                                <td><a class="player-table-values" href="">Rathanvel, V S</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2351</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">123</td>
                                <td><a class="player-table-values" href="">Kaustuv, Kundu</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2351</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">124</td>
                                <td><a class="player-table-values" href="">Sidhant,
                                        Mohapatra</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2349</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">125</td>
                                <td><a class="player-table-values" href="">Roy, Prantik</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2347</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">126</td>
                                <td><a class="player-table-values" href="">Srijit Paul</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2347</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">127</td>
                                <td><a class="player-table-values" href="">Ravikumar,
                                        Vaidyanathan</a></td>
                                <td class="player-table-value">IM,&nbsp;FA</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2347</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">128</td>
                                <td><a class="player-table-values" href="">Sareen, Vishal</a>
                                </td>
                                <td class="player-table-value">IM,&nbsp;FST</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2346</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">129</td>
                                <td><a class="player-table-values" href="">Anand Nadar</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2346</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">130</td>
                                <td><a class="player-table-values" href="">Gauri, Shankar</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2345</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">131</td>
                                <td><a class="player-table-values" href="">Saravana, Krishnan
                                        P.</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2344</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">132</td>
                                <td><a class="player-table-values" href="">Raja Rithvik R</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2343</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">133</td>
                                <td><a class="player-table-values" href="">Sankalp Gupta</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2341</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">134</td>
                                <td><a class="player-table-values" href="">Siva Mahadevan</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2341</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">135</td>
                                <td><a class="player-table-values" href="">Konguvel,
                                        Ponnuswamy</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2340</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">136</td>
                                <td><a class="player-table-values" href="">Sammed
                                        Jaykumar,Shete</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2339</td>
                                <td class="player-table-value">Processing</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">137</td>
                                <td><a class="player-table-values" href="">Padmini, Rout</a>
                                </td>
                                <td class="player-table-value">IM,&nbsp;WGM</td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2338</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">138</td>
                                <td><a class="player-table-values" href="">Roy, Shankar</a></td>
                                <td class="player-table-value">IA</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2337</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">139</td>
                                <td><a class="player-table-values" href="">Vaishali R</a></td>
                                <td class="player-table-value">WIM,&nbsp;WIM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2337</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">140</td>
                                <td><a class="player-table-values" href="">Kulkarni, Chinmay</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2336</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">141</td>
                                <td><a class="player-table-values" href="">Kulkarni,
                                        Vikramaditya</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2334</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">142</td>
                                <td><a class="player-table-values" href="">Parameswaran, Tiruchi
                                        N.</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2334</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">143</td>
                                <td><a class="player-table-values" href="">Anilkumar, N R</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2334</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">144</td>
                                <td><a class="player-table-values" href="">Sanjay, N.</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2330</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">145</td>
                                <td><a class="player-table-values" href="">Wagh Suyog</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2328</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">146</td>
                                <td><a class="player-table-values" href="">Pavanasam,
                                        Ananchaperumal</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2327</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">147</td>
                                <td><a class="player-table-values" href="">Saravanan, V.</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2326</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">148</td>
                                <td><a class="player-table-values" href="">Abhishek, Das</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Jharkhand</td>
                                <td class="player-table-value">2325</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">149</td>
                                <td><a class="player-table-values" href="">Thakur, Akash</a>
                                </td>
                                <td class="player-table-value">FM,&nbsp;FT</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2325</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">150</td>
                                <td><a class="player-table-values" href="">Kathmale, Sameer</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2323</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">151</td>
                                <td><a class="player-table-values" href="">Barath M</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Goa</td>
                                <td class="player-table-value">2321</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">152</td>
                                <td><a class="player-table-values" href="">Natarajan S V</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2320</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">153</td>
                                <td><a class="player-table-values" href="">Neelash Saha</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2319</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">154</td>
                                <td><a class="player-table-values" href="">Sharma, R.
                                        Preetham</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2318</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">155</td>
                                <td><a class="player-table-values" href="">Aaron, Manuel</a>
                                </td>
                                <td class="player-table-value">IM,&nbsp;IA</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2315</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">156</td>
                                <td><a class="player-table-values" href="">Aakanksha
                                        Hagawane</a></td>
                                <td class="player-table-value">WIM,&nbsp;WIM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2314</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">157</td>
                                <td><a class="player-table-values" href="">Sahoo, Utkal
                                        Ranjan</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Orissa</td>
                                <td class="player-table-value">2313</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">158</td>
                                <td><a class="player-table-values" href="">Girinath, P.D.S.</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2313</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">159</td>
                                <td><a class="player-table-values" href="">Gokhale, Jayant
                                        Suresh</a></td>
                                <td class="player-table-value">FT</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2312</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">160</td>
                                <td><a class="player-table-values" href="">Shankar, Roy</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2311</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">161</td>
                                <td><a class="player-table-values" href="">Aronyak Ghosh</a>
                                </td>
                                <td class="player-table-value">CM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2310</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">162</td>
                                <td><a class="player-table-values" href="">Aaryan Varshney</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2309</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">163</td>
                                <td><a class="player-table-values" href="">Kushagra Mohan</a>
                                </td>
                                <td class="player-table-value">CM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2309</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">164</td>
                                <td><a class="player-table-values" href="">Rajesh, V A V</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2309</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">165</td>
                                <td><a class="player-table-values" href="">Nitish Belurkar</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Goa</td>
                                <td class="player-table-value">2306</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">166</td>
                                <td><a class="player-table-values" href="">Kakumanu Kautil</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2306</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">167</td>
                                <td><a class="player-table-values" href="">Premnath, R.</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2306</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">168</td>
                                <td><a class="player-table-values" href="">Parappalli, Joe</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2305</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">169</td>
                                <td><a class="player-table-values" href="">Pranav Anand</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Karnataka</td>
                                <td class="player-table-value">2305</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">170</td>
                                <td><a class="player-table-values" href="">Navin, Kanna T.u.</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2304</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">171</td>
                                <td><a class="player-table-values" href="">Ravi, Lanka</a></td>
                                <td class="player-table-value">IM,&nbsp;FA,FST</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2301</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">172</td>
                                <td><a class="player-table-values" href="">Acharya, Chandragupta
                                        K.</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2300</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">173</td>
                                <td><a class="player-table-values" href="">Mithil, Ajgaonkar</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2300</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">174</td>
                                <td><a class="player-table-values" href="">Roy Chowdhury,
                                        Saptarshi</a></td>
                                <td class="player-table-value">GM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2299</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">175</td>
                                <td><a class="player-table-values" href="">Mitrabha, Guha</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2299</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">176</td>
                                <td><a class="player-table-values" href="">Kumaran, B</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2298</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">177</td>
                                <td><a class="player-table-values" href="">Abhay, T.</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2297</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">178</td>
                                <td><a class="player-table-values" href="">Ramnath
                                        Bhuvanesh.R</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2296</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">179</td>
                                <td><a class="player-table-values" href="">Vantika Agrawal</a>
                                </td>
                                <td class="player-table-value">WIM,&nbsp;WIM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2295</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">180</td>
                                <td><a class="player-table-values" href="">Karthik Kumar
                                        Pradeep</a></td>
                                <td class="player-table-value">CM</td>
                                <td class="player-table-value">Telangana</td>
                                <td class="player-table-value">2295</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">181</td>
                                <td><a class="player-table-values" href="">Ravi, Thandalam
                                        Shanmugam</a></td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2294</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">182</td>
                                <td><a class="player-table-values" href="">Adithya B</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2293</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">183</td>
                                <td><a class="player-table-values" href="">Raahul V S</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2292</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">184</td>
                                <td><a class="player-table-values" href="">Monnisha G K</a></td>
                                <td class="player-table-value">WIM,&nbsp;WIM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2292</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">185</td>
                                <td><a class="player-table-values" href="">Debarshi,
                                        Mukherjee</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2292</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">186</td>
                                <td><a class="player-table-values" href="">Dixit, Nikhil</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2292</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">187</td>
                                <td><a class="player-table-values" href="">Raghavi, N.</a></td>
                                <td class="player-table-value">WIM,&nbsp;WIM</td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2290</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">188</td>
                                <td><a class="player-table-values" href="">Sangma, Rahul</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2290</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">189</td>
                                <td><a class="player-table-values" href="">Shailesh, Dravid</a>
                                </td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2290</td>
                                <td class="player-table-value">Processing</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">190</td>
                                <td><a class="player-table-values" href="">Beenish, Bhatia</a>
                                </td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2289</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">191</td>
                                <td><a class="player-table-values" href="">Lahiri, Atanu</a>
                                </td>
                                <td class="player-table-value">IM,&nbsp;FT</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2288</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">192</td>
                                <td><a class="player-table-values" href="">Parag, Varde</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">2287</td>
                                <td class="player-table-value">N/A</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">193</td>
                                <td><a class="player-table-values" href="">Mehar, Chinna Reddy
                                        C.H.</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Andhra Pradesh</td>
                                <td class="player-table-value">2286</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">194</td>
                                <td><a class="player-table-values" href="">Kashelkar,
                                        Ramkrishna</a></td>
                                <td class="player-table-value">FM</td>
                                <td class="player-table-value">Maharashtra</td>
                                <td class="player-table-value">2285</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">195</td>
                                <td><a class="player-table-values" href="">Prasad, Devaki V</a>
                                </td>
                                <td class="player-table-value">IM</td>
                                <td class="player-table-value">Others</td>
                                <td class="player-table-value">2285</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">196</td>
                                <td><a class="player-table-values" href="">Ashwath, R.</a></td>
                                <td class="player-table-value"></td>
                                <td class="player-table-value">Tamil Nadu</td>
                                <td class="player-table-value">2285</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">197</td>
                                <td><a class="player-table-values" href="">Kulkarni Bhakti</a>
                                </td>
                                <td class="player-table-value">CM,&nbsp;WGM</td>
                                <td class="player-table-value">Goa</td>
                                <td class="player-table-value">2285</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">198</td>
                                <td><a class="player-table-values" href="">Prasenjit Dutta</a>
                                </td>
                                <td class="player-table-value">FM,&nbsp;FA</td>
                                <td class="player-table-value">Delhi</td>
                                <td class="player-table-value">2285</td>
                                <td class="player-table-value">Inactive</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">199</td>
                                <td><a class="player-table-values" href="">Gomes, Mary Ann</a>
                                </td>
                                <td class="player-table-value">WGM,&nbsp;WGM</td>
                                <td class="player-table-value">West Bengal</td>
                                <td class="player-table-value">2284</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Female</td>
                            </tr>
                            <tr>
                                <td class="player-table-value">200</td>
                                <td><a class="player-table-values" href="">Sharma, Dinesh K.</a>
                                </td>
                                <td class="player-table-value">IM,&nbsp;FT</td>
                                <td class="player-table-value">Uttar Pradesh</td>
                                <td class="player-table-value">2282</td>
                                <td class="player-table-value">Active</td>
                                <td class="player-table-value">Male</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                    <form>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 form-group padd_top_20">
                            <input type="text" class="form-control" placeholder="Player Name / FIDE ID / AICF ID"
                                   name="q" id="usr">

                            <select class="form-control" id="sel1">
                                <option value="0">Select a Genter</option>
                                <option value="M">Male</option>
                                <option value="F">Female</option>
                            </select>

                            <select class="form-control" id="sel1">
                                <option>Select a State</option>
                                <option value="0">Select a State</option>
                                <option value="1">Delhi</option>
                                <option value="2">Tamil Nadu</option>
                                <option value="3">Andhra Pradesh</option>
                                <option value="4">Arunachal Pradesh</option>
                                <option value="5">Assam</option>
                                <option value="6">Bihar</option>
                                <option value="7">Chhattisgarh</option>
                                <option value="8">Goa</option>
                                <option value="9">Gujarat</option>
                                <option value="10">Himachal Pradesh</option>
                                <option value="11">Haryana</option>
                                <option value="12">Jammu and Kashmir</option>
                                <option value="13">Jharkhand</option>
                                <option value="14">Karnataka</option>
                                <option value="15">Kerala</option>
                                <option value="16">Madhya Pradesh</option>
                                <option value="17">Maharashtra</option>
                                <option value="18">Manipur</option>
                                <option value="19">Meghalaya</option>
                                <option value="20">Mizoram</option>
                                <option value="21">Orissa</option>
                                <option value="22">Nagaland</option>
                                <option value="23">Punjab</option>
                                <option value="24">Rajasthan</option>
                                <option value="25">Sikkim</option>
                                <option value="26">Tripura</option>
                                <option value="27">Uttrakhand</option>
                                <option value="28">Uttar Pradesh</option>
                                <option value="29">West Bengal</option>
                                <option value="30">Andaman and Nicobar Islands</option>
                                <option value="31">Chandigarh</option>
                                <option value="32">Dadar and Nagar Haveli</option>
                                <option value="33">Lakshadeep</option>
                                <option value="34">Pondicherry</option>
                                <option value="35">Daman and Diu</option>
                                <option value="36">Telangana</option>
                                <option value="37">Others</option>
                            </select>

                            <select class="form-control" id="sel1">
                                <option value="0">Select a Type</option>
                                <option value="p">Player</option>
                                <option value="a">Arbiter</option>
                                <option value="c">Coach</option>
                            </select>
                            <select class="form-control" id="sel1">
                                <option value="0">Select a Title</option>
                                <option value="GM">GM</option>
                                <option value="WGM">WGM</option>
                                <option value="FT">FT</option>
                                <option value="IM">IM</option>
                                <option value="FST">FST</option>
                                <option value="FA">FA</option>
                                <option value="IA">IA</option>
                                <option value="DI">DI</option>
                                <option value="FM">FM</option>
                                <option value="FI">FI</option>
                                <option value="NI">NI</option>
                                <option value="CM">CM</option>
                                <option value="WIM">WIM</option>
                                <option value="WFM">WFM</option>
                                <option value="WCM">WCM</option>
                            </select>

                            <select class="form-control" id="sel1">
                                <option value="0">Select a Reg Status</option>
                                <option value="y">Active</option>
                                <option value="p">Processing</option>
                                <option value="n">Inactive</option>
                            </select>
                            <button type="button" class="btn btn-warning">Filter</button>

                            <input class="btn btn-info" onclick="loadpage();" value="Clear Filter" name="clear" class="event_filter_button"
                                   type="reset">
                        </div>
                    </form>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                        <div class="widget-textarea padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/player1.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/player-search/"> Player Search </a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/chess_schools.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href=""> Chess in Schools</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/cash.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/cashawards/"> Cash Awards</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/rating.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/ratingquery/">Rating Query Online</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/arbiter.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/arbitercorner/">Arbiter Corner</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10">
                        </div>
                        <div class="widget-textarea padd_left_right_none padd_botton_10 padd_top_10 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-2 col-md-2 col-sm-2 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/aicf-top.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class="padd_left_right_none col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                <a class="player_search" href="http://dev-cbe-chess.pantheonsite.io/oci-card-holder/">PIO / OCI card holders</a>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  padd_left_right_none padd_botton_10 padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <a href="http://dev-cbe-chess.pantheonsite.io/twin-international/"> <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/goa.jpg" alt="player"> </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/national-reg/">NATIONAL TOURNAMENT – <br>24 x 7 assistance is arranged towards confirmation of <br> entry : Phone numbers : +917358534422 / +918610193178</a> </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/aicf-payments/"> CLICK HERE FOR VARIOUS
                                    PAYMENTS </a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="http://dev-cbe-chess.pantheonsite.io/faq/">FAQ</a>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_10">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                News
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/news.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-11: Shreyash and Savitha </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/news1.jpeg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> National Under-07: Amogh and Lakshana </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12
                             latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/thumbnail.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under-15: Ajay, Divya crowned </a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Featured Events
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_5">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features4.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href=""> 32nd
                                    National Under – 11 (Open &amp; Girls)</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features3.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Cities – 2018  – 189826 / WB / 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features2.jpg"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">National Under – 25 <br> (Open  &amp; Girls) – 2018</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                    <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/features1.png"
                                         alt="player">
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 events-title" href="">Goa International GM Open 2018 – 186616</a>
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 latest-news-meta">
                                    <div class="news-date-meta"><span class="latest-news-date"><i
                                                    class="fa fa-clock-o"></i> 31 Jul 2018</span></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none widget-textarea_right ">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 live" href="">Title Applications</a>
                            </div>
                        </div>

                        <div class="widget-textareass padd_left_right_none col-lg-12 padd_botton_10 padd_top_10 col-md-12 col-sm-12 col-xs-12">
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                            <div class=" col-lg-10 col-md-10 col-sm-10 col-xs-12 padd_left_right_none">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 player_searchss"> INDIA HIGHLIGHTS </div>
                            </div>
                            <div class="padd_left_right_none  col-lg-1 col-md-1 col-sm-1 col-xs-12">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/india.png"
                                     class="player_img" alt="player">
                            </div>
                        </div>


                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>76612</strong>
                                    </div>
                                    <span>Registered Players</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>28574</strong>
                                    </div>
                                    <span>FIDE Rated Players</span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_15">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>219</strong>
                                    </div>
                                    <span>Tournaments in 2018</span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <strong>51</strong>
                                    </div>
                                    <span> Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none  padd_top_20">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>101</strong>
                                    </div>
                                    <span> International Masters </span>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 padd_left_right_none">
                                <div id="circle">
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_20">
                                        <strong>7</strong>
                                    </div>
                                    <span> Women Grand Masters </span>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                AICF Chronicles
                            </div>
                        </div>
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_botton_10 events-titless " href="">July 2018</a>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/AICF.jpg">
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none padd_top_20">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 announcements widget-india">
                                Mate in two moves
                            </div>
                        </div>

                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_left_right_none">
                                <img src="http://dev-cbe-chess.pantheonsite.io/wp-content/uploads/2018/08/chee_buttom.jpg">
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


<?php
get_footer();
?>